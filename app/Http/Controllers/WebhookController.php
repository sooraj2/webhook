<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Webhook;
class WebhookController extends Controller
{
    public function handle(Request $request)
    {
        $name = $request->get('first_name');
        $webhook = new Webhook([
            'messagetext' => $name.'created new contact'
        ]);
        $webhook->save();
    }

}
